class CreateFees < ActiveRecord::Migration[5.2]
  def change
    create_table :fees do |t|
      t.references :origin
      t.references :destination
      t.decimal :value

      t.timestamps
    end

    add_foreign_key :fees, :locality, column: :origin_id, primary_key: :id
    add_foreign_key :fees, :locality, column: :destination_id, primary_key: :id

  end
end

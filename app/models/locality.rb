class Locality < ApplicationRecord
  has_many :origin_fees, :class_name => 'Fee', :foreign_key => 'origin_id'
  has_many :destination_fees, :class_name => 'Fee', :foreign_key => 'destination_id'
end

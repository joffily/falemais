class Fee < ApplicationRecord
  belongs_to :origin, class_name: 'Locality'
  belongs_to :destination, class_name: 'Locality'
end
